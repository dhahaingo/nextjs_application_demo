#build base
FROM node:16-alpine as BASE
# LABEL author="DHAHaiNgo <hai_ngo@datahouse.com>"

WORKDIR /app

COPY package.json yarn.lock ./
RUN apk add --no-cache git \
    && yarn install --frozen-lockfile \
    && yarn cache clean

#build image
FROM node:16-alpine AS BUILD
# LABEL author="DHAHaiNgo <hai_ngo@datahouse.com>"

WORKDIR /app
COPY --from=BASE /app/node_modules ./node_modules
COPY . .

#install node_prune
RUN apk add --no-cache curl \ 
    && curl -sf https://gobinaries.com/tj/node-prune | sh -s -- -b /usr/local/bin \
    && apk del curl 

RUN apk add --no-cache git curl \
    && yarn build \
    && rm -rf node_modules \
    && yarn install --production --frozen-lockfile --ignore-scripts --prefer-offline \
    && node-prune


# Build production
FROM node:16-alpine AS PRODUCTION
# LABEL author="DHAHaiNgo <hai_ngo@datahouse.com>"

WORKDIR /app

COPY --from=BUILD /app/package.json /app/yarn.lock ./
COPY --from=BUILD /app/node_modules ./node_modules
COPY --from=BUILD /app/.next ./.next
COPY --from=BUILD /app/public ./public

EXPOSE 3000

CMD ["yarn", "start"]