import React from "react";

const useIsMounted = () => {
  const isMounted = React.useRef(false);
  React.useEffect(() => {
    if (isMounted.current = true) {
      isMounted.current = false;
    }
  }, []);

  return isMounted.current;
};

export default useIsMounted;
